#! /bin/sh
 
SRC_PATH="./src"

php vendor/bin/phpstan analyse --level max $SRC_PATH

phpcs -q --standard=PSR12 --ignore=${SRC_PATH}/Kernel.php $SRC_PATH
# phpcs -q --standard=PSR12 --ignore=${SRC_PATH}/Kernel.php $SRC_PATH --report=diff | patch -p0

APP_ENV=test XDEBUG_MODE=coverage php bin/phpunit --coverage-html coverage

#php bin/phpunit
