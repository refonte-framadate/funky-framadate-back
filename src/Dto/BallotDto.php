<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

class BallotDto
{
    /** Pseudo */
    #[Groups(['post'])]
    public ?string $pseudo = null;

    /**
     * Array of votes
     *
     * @var array<VoteDto>
     */
    #[Assert\Type('array')]
    #[Groups(['post'])]
    public ?array $votes;
}
