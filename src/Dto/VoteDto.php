<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

class VoteDto
{
    /** Slot's ID */
    #[Groups(['post'])]
    public ?int $slotId = null;

    /** Value */
    #[Groups(['post'])]
    public ?string $value = null;
}
