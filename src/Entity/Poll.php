<?php

/**
 * Some docs...
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Repository\PollRepository;
use App\Dto\SlotArrayDto;
use App\State\SlotArrayProcessor;
use App\Dto\BallotDto;
use App\State\BallotProcessor;

#[ORM\Entity(repositoryClass: PollRepository::class)]
#[UniqueEntity('slug')]
#[Get(
    uriVariables: 'slug',
    normalizationContext: ['groups' => ['getItem']]
)]
#[Post(
    normalizationContext: ['groups' => ['getItem']],
    denormalizationContext: ['groups' => ['post']]
)]
#[Post(
    openapiContext: [
        'summary' => 'Add one or more slots to a poll.',
        'description' => 'Add one or more slots to a poll.'
    ],
    uriTemplate: '/polls/{slug}/slots',
    uriVariables: 'slug',
    input: SlotArrayDto::class,
    processor: SlotArrayProcessor::class,
    normalizationContext: ['groups' => ['getItem']],
    denormalizationContext: ['groups' => ['post']]
)]
#[Post(
    openapiContext: [
        'summary' => 'Add a ballot to a poll.',
        'description' => 'Add a ballot to a poll.'
    ],
    uriTemplate: '/polls/{slug}/ballots',
    uriVariables: 'slug',
    input: BallotDto::class,
    processor: BallotProcessor::class,
    normalizationContext: ['groups' => ['getItem']],
    denormalizationContext: ['groups' => ['post']]
)]
class Poll
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    /**
     * Slug
     *
     * Poll unique identifier
     * @var string
     */
    #[ORM\Column(length: 255, unique: true)]
    #[Groups(['getItem', 'post'])]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $slug = null;

    /** Title */
    #[ORM\Column(length: 255)]
    #[Groups(['getItem', 'post'])]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $title = null;

    /** Description */
    #[ORM\Column(length: 1023, nullable: true)]
    #[Groups(['getItem', 'post'])]
    #[Assert\Length(max: 1023)]
    private ?string $description = null;

    /**
     * Type
     *
     * Must be an element of array<string> getTypes()
     */
    #[ORM\Column(length: 15)]
    #[Groups(['getItem', 'post'])]
    #[Assert\NotBlank]
    #[Assert\Choice(callback: 'getTypes')]
    #[Assert\Length(max: 15)]
    #[ApiProperty(
        openapiContext: [
            'enum' => ['date', 'classic']
        ]
    )]
    private ?string $type = null;

    /** Creator's name */
    #[ORM\Column(length: 255)]
    #[Groups(['getItem', 'post'])]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private ?string $creatorName = null;

    /** Creator's email */
    #[ORM\Column(length: 255)]
    #[Groups(['getItem', 'post'])]
    #[Assert\NotBlank]
    #[Assert\Email]
    #[Assert\Length(max: 255)]
    private ?string $creatorMail = null;

    /**
     * Proposals
     *
     * @var Collection<Slot>
     */
    #[ORM\OneToMany(mappedBy: 'poll', targetEntity: Slot::class, orphanRemoval: true, cascade: ['all'])]
    #[Groups(['getItem', 'post'])]
    // #[Assert\Type('Collection')]
    #[Assert\All([
        new Assert\Type('App\Entity\Slot')
    ])]
    #[Assert\NotBlank]
    // #[Assert\Valid]
    private Collection $slots;

    /**
     * Participants
     *
     * @var Collection<Participant>
     */
    #[ORM\OneToMany(mappedBy: 'poll', targetEntity: Participant::class, orphanRemoval: true, cascade: ['all'])]
    #[Groups(['getItem'])]
    private Collection $participants;

    /**
     * Votes
     *
     * @var Collection<Vote>
     */
    #[ORM\OneToMany(mappedBy: 'poll', targetEntity: Vote::class, orphanRemoval: true, cascade: ['all'])]
    #[Groups(['getItem'])]
    private Collection $votes;

    public function __construct()
    {
        $this->slots = new ArrayCollection();
        $this->participants = new ArrayCollection();
        $this->votes = new ArrayCollection();
    }

    /**
     * The possible valid types of a poll
     *
     * @return array<string>
     */
    public static function getTypes(): array
    {
        return ['date', 'classic'];
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getSlug(): ?string
    {
        return $this->slug;
    }


    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }


    public function getTitle(): ?string
    {
        return $this->title;
    }


    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }


    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getType(): ?string
    {
        return $this->type;
    }


    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }


    public function getCreatorName(): ?string
    {
        return $this->creatorName;
    }


    public function setCreatorName(string $creatorName): self
    {
        $this->creatorName = $creatorName;

        return $this;
    }


    public function getCreatorMail(): ?string
    {
        return $this->creatorMail;
    }


    public function setCreatorMail(string $creatorMail): self
    {
        $this->creatorMail = $creatorMail;

        return $this;
    }

    /**
     * @return Collection<int, Slot>
     */
    public function getSlots(): Collection
    {
        return $this->slots;
    }

    public function getSlotById(int $id): null|Slot
    {
        foreach ($this->slots as $slot) {
            if ($slot->getId() == $id) {
                return $slot;
            }
        }
        return null;
    }

    public function addSlot(Slot $slot): self
    {
        if (!$this->slots->contains($slot)) {
            $this->slots->add($slot);
            $slot->setPoll($this);
        }

        return $this;
    }

    public function removeSlot(Slot $slot): self
    {
        if ($this->slots->removeElement($slot)) {
            // set the owning side to null (unless already changed)
            if ($slot->getPoll() === $this) {
                $slot->setPoll(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Participant>
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(Participant $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
            $participant->setPoll($this);
        }

        return $this;
    }

    public function removeParticipant(Participant $participant): self
    {
        if ($this->participants->removeElement($participant)) {
            // set the owning side to null (unless already changed)
            if ($participant->getPoll() === $this) {
                $participant->setPoll(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Vote>
     */
    public function getVotes(): Collection
    {
        return $this->votes;
    }

    public function addVote(Vote $vote): self
    {
        if (!$this->votes->contains($vote)) {
            $this->votes->add($vote);
            $vote->setPoll($this);
        }

        return $this;
    }

    public function removeVote(Vote $vote): self
    {
        if ($this->votes->removeElement($vote)) {
            // set the owning side to null (unless already changed)
            if ($vote->getPoll() === $this) {
                $vote->setPoll(null);
            }
        }

        return $this;
    }
}
