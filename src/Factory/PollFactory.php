<?php

namespace App\Factory;

use App\Entity\Poll;
use App\Repository\PollRepository;
use App\Factory\SlotFactory;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Poll>
 *
 * @method        Poll|Proxy create(array|callable $attributes = [])
 * @method static Poll|Proxy createOne(array $attributes = [])
 * @method static Poll|Proxy find(object|array|mixed $criteria)
 * @method static Poll|Proxy findOrCreate(array $attributes)
 * @method static Poll|Proxy first(string $sortedField = 'id')
 * @method static Poll|Proxy last(string $sortedField = 'id')
 * @method static Poll|Proxy random(array $attributes = [])
 * @method static Poll|Proxy randomOrCreate(array $attributes = [])
 * @method static PollRepository|RepositoryProxy repository()
 * @method static Poll[]|Proxy[] all()
 * @method static Poll[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Poll[]|Proxy[] createSequence(iterable|callable $sequence)
 * @method static Poll[]|Proxy[] findBy(array $attributes)
 * @method static Poll[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static Poll[]|Proxy[] randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<Poll> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<Poll> createOne(array $attributes = [])
 * @phpstan-method static Proxy<Poll> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<Poll> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<Poll> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<Poll> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<Poll> random(array $attributes = [])
 * @phpstan-method static Proxy<Poll> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<Poll> repository()
 * @phpstan-method static list<Proxy<Poll>> all()
 * @phpstan-method static list<Proxy<Poll>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<Poll>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<Poll>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<Poll>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<Poll>> randomSet(int $number, array $attributes = [])
 */
final class PollFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'creatorMail' => self::faker()->email(),
            'creatorName' => self::faker()->name(),
            'slug'        => self::faker()->slug(20),
            'title'       => self::faker()->text(20),
            'description' => self::faker()->text(200),
            'type'        => self::faker()->randomElement(Poll::getTypes()),
            'slots'        => []
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Poll $poll): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Poll::class;
    }

    /**
     * Create a populated poll
     *
     * @param $params['title']    Poll's title
     * @param $params['slug']     Poll's slug
     * @param $params['nb_slot']  Number of slots to create for this poll
     */
    public static function createPopulatedPoll(array $params = null): Poll
    {
        $pollParam = [];
        $nb_slot = 5;
        $nb_participant = 3;
        if ($params !== null) {
            if (array_key_exists('title', $params)) {
                $pollParam['title'] = $$params['title'];
            }
            if (array_key_exists('slug', $params)) {
                $pollParam['slug'] = $params['slug'];
            }
            if (array_key_exists('nb_slot', $params)) {
                $nb_slot = $params['nb_slot'];
            }
            if (array_key_exists('nb_participant', $params))
            {
                $nb_participant = $params['nb_participant'];
            }
        }
        $poll = PollFactory::createOne($pollParam);
        $slots = SlotFactory::createMany($nb_slot, ['poll' => $poll]);
        $participants = ParticipantFactory::createMany($nb_participant, ['poll' => $poll]);
        foreach($participants as $participant)
        {
            foreach($slots as $slot)
            {
                VoteFactory::createOne(['poll' => $poll, 'slot' => $slot, 'participant' => $participant]);
            }
        }

        return $poll->object();
    }
}
