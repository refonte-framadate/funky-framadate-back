<?php

namespace App\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Zenstruck\Foundry\Test\ResetDatabase;

use \App\Entity\Poll;
use App\Factory\PollFactory;
use App\Repository\PollRepository;

class PollKernelTest extends KernelTestCase
{
    use ResetDatabase;

    public function testSomething(): void
    {
        $kernel = self::bootKernel();
        $pollRepo = static::getContainer()->get(PollRepository::class);

        $post = PollFactory::createOne(['slug' => 'must-be-unique', 'title' => '']);
        $post->assertPersisted();
        PollFactory::assert()->count(1);
        PollFactory::assert()->exists(['slug' => 'must-be-unique']);
        $this->assertEquals(1, sizeof($pollRepo->findBy(['slug' => 'must-be-unique'])));

        $this->expectException(UniqueConstraintViolationException::class);
        // $this->expectExceptionMessage('');

        PollFactory::createOne(['slug' => 'must-be-unique']);

        $this->assertEquals(1, sizeof($pollRepo->findAll()));

        // $this->assertSame('test', $kernel->getEnvironment());
        // $routerService = static::getContainer()->get('router');
    }
}
