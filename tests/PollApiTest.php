<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

use Zenstruck\Foundry\Test\ResetDatabase;

use App\Entity\Poll;
use App\Factory\PollFactory;

class PollApiTest extends ApiTestCase
{
    use ResetDatabase;

    public function testPollGetCollection(): void
    {
        $response = static::createClient()->request('GET', '/polls', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
        ]);

        $this->assertResponseStatusCodeSame(405);
    }

    public function testPollGetItem(): void
    {
        PollFactory::createOne(["slug" => 'testing-poll']);

        $response = static::createClient()->request('GET', '/polls/testing-poll', [
            'headers' => [
                'Accept' => 'application/json'
            ]
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['slug' => 'testing-poll']);
        $this->assertMatchesResourceItemJsonSchema(Poll::class);
    }

    public function testPollPostHtmlItem(): void
    {
        $response = static::createClient()->request('POST', '/polls', [
            'headers' => [
                'Accept' => 'text/html',
                'Accept' => 'application/json',
                'Content-Type' => 'text/html'
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testPollPostEmptyItem(): void
    {
        $response = static::createClient()->request('POST', '/polls', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'json' => []
        ]);

        $this->assertResponseStatusCodeSame(422);
    }

    public function testPollPostUnprocessableItem(): void
    {
        // title
        $response = static::createClient()->request('POST', '/polls', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'title' => '',
                'slug' => 'frsggr',
                'description' => 'string',
                'type' => 'date',
                'creatorName' => 'string',
                'creatorMail' => 'user@example.com'
                ]
        ]);
        $this->assertResponseStatusCodeSame(422);

        // empty slug
        $response = static::createClient()->request('POST', '/polls', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'title' => 'a title',
                'slug' => '',
                'description' => 'string',
                'type' => 'classic',
                'creatorName' => 'string',
                'creatorMail' => 'user@example.com'
                ]
        ]);
        $this->assertResponseStatusCodeSame(422);

        // type
        $response = static::createClient()->request('POST', '/polls', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'title' => 'a poll with a bad type',
                'slug' => 'bad-type',
                'description' => 'string',
                'type' => 'string',
                'creatorName' => 'string',
                'creatorMail' => 'user@example.com'
                ]
        ]);
        $this->assertResponseStatusCodeSame(422);

        // email
        $response = static::createClient()->request('POST', '/polls', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'title' => 'a title',
                'slug' => 'invalid-email',
                'description' => 'string',
                'type' => 'date',
                'creatorName' => 'string',
                'creatorMail' => 'invalid'
                ]
        ]);
        $this->assertResponseStatusCodeSame(422);

        // slug duplicate
        $response = static::createClient()->request('POST', '/polls', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'title' => 'a title',
                'slug' => 'a-title',
                'description' => 'string',
                'type' => 'date',
                'creatorName' => 'string',
                'creatorMail' => 'user@example.com',
                'slots' => []
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $response = static::createClient()->request('POST', '/polls', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'title' => 'a title',
                'slug' => 'a-title',
                'description' => 'string',
                'type' => 'date',
                'creatorName' => 'string',
                'creatorMail' => 'user@example.com'
                ]
        ]);
        $this->assertResponseStatusCodeSame(422);

    }
}
